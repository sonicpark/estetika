from django.contrib import admin

from .models.pluginmodel.testimonials import Testimonial


@admin.register(Testimonial)
class TestimonialModelAdmin(admin.ModelAdmin):
    pass
