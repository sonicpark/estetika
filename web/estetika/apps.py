from django.apps import AppConfig


class EstetikaConfig(AppConfig):
    name = 'estetika'
    verbose_name = 'Эстетика'
