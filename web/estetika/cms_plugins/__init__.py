from . import (
    slider,
    social,
    banner,
    testimonials,
    services,
    about,
    footer,
    tabs,
    gallery,
    curved,
    featured_courses,
)
