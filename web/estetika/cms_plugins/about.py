from cms.plugin_pool import CMSPluginBase, plugin_pool
from django.utils.translation import ugettext_lazy as _

from ..models.pluginmodel.about import AboutPluginModel


@plugin_pool.register_plugin
class AboutPlugin(CMSPluginBase):
    model = AboutPluginModel
    name = _('О нас')
    render_template = 'plugins/about.html'
    cache = False
    allow_children = False

    def render(self, context, instance, placeholder):
        context = super(AboutPlugin, self).render(context, instance, placeholder)
        return context
