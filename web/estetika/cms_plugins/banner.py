from cms.plugin_pool import CMSPluginBase, plugin_pool
from django.utils.translation import ugettext_lazy as _

from ..models.pluginmodel.banner import PageBanner


@plugin_pool.register_plugin
class PageBannerPlugin(CMSPluginBase):
    model = PageBanner
    name = _('Статический банер')
    render_template = 'plugins/banner.html'
    cache = True,
    allow_children = False

    def render(self, context, instance, placeholder):
        context = super(PageBannerPlugin, self).render(context, instance, placeholder)
        return context
