from cms.plugin_pool import CMSPluginBase, plugin_pool
from django.utils.translation import ugettext_lazy as _

from ..models.pluginmodel.curved import CurvedSection


@plugin_pool.register_plugin
class CurvedSectionPlugin(CMSPluginBase):
    model = CurvedSection
    name = _('Косая Секция')
    render_template = 'plugins/curved.html'
    cache = False
    allow_children = False

    def render(self, context, instance, placeholder):
        context = super(CurvedSectionPlugin, self).render(context, instance, placeholder)
        return context
