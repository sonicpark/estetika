from cms.plugin_pool import CMSPluginBase, plugin_pool
from django.utils.translation import ugettext_lazy as _

from ..models.pluginmodel.featured_courses import Featured, FeaturedPost


@plugin_pool.register_plugin
class FeaturedPlugin(CMSPluginBase):
    model = Featured
    name = _('Список услуг')
    render_template = 'plugins/featured.html'
    cache = False
    allow_children = True
    child_classes = ['FeaturedPostPlugin']

    def render(self, context, instance, placeholder):
        context = super(FeaturedPlugin, self).render(context, instance, placeholder)
        return context


@plugin_pool.register_plugin
class FeaturedPostPlugin(CMSPluginBase):
    model = FeaturedPost
    name = _('Список услуг: Пост')
    render_template = 'plugins/featured_post.html'
    cache = False
    require_parent = True
    allow_children = False
    parent_classes = ['FeaturedPlugin']

    def render(self, context, instance, placeholder):
        context = super(FeaturedPostPlugin, self).render(context, instance, placeholder)
        return context
