from cms.plugin_pool import CMSPluginBase, plugin_pool
from django.utils.translation import ugettext_lazy as _

from ..models.pluginmodel.footer import (
    CustomFooter,
    CustomFooterUpper,
    CustomFooterLower,
    CustomFooterBrand,
    CustomFooterBrandLink,
    CustomFooterContactUs,
    CustomFooterQuickLinks,
    CustomFooterQuickLinksList,
    CustomFooterQuickLinksLink,
)


@plugin_pool.register_plugin
class CustomFooterPlugin(CMSPluginBase):
    model = CustomFooter
    name = _('Футер')
    render_template = 'plugins/footer.html'
    cache = True
    allow_children = True

    child_classes = [
        'CustomFooterUpperPlugin',
        'CustomFooterLowerPlugin',
    ]

    def render(self, context, instance, placeholder):
        context = super(CustomFooterPlugin, self).render(context, instance, placeholder)
        return context


@plugin_pool.register_plugin
class CustomFooterUpperPlugin(CMSPluginBase):
    model = CustomFooterUpper
    name = _('Футер: Верхняя часть')
    render_template = 'plugins/footer_upper.html'
    cache = True
    allow_children = True

    child_classes = [
        'SocialNetworksPlugin',
    ]

    parent_classes = [
        'CustomFooterPlugin'
    ]

    def render(self, context, instance, placeholder):
        context = super(CustomFooterUpperPlugin, self).render(context, instance, placeholder)
        return context


@plugin_pool.register_plugin
class CustomFooterLowerPlugin(CMSPluginBase):
    model = CustomFooterLower
    name = _('Футер: Нижняя часть')
    render_template = 'plugins/footer_lower.html'
    cache = True
    allow_children = True

    child_classes = [
        'CustomFooterBrandPlugin',
        'CustomFooterContactPlugin',
        'CustomFooterQuickLinksPlugin',
    ]

    parent_classes = [
        'CustomFooterPlugin',
    ]


@plugin_pool.register_plugin
class CustomFooterBrandPlugin(CMSPluginBase):
    model = CustomFooterBrand
    name = _('Бренд')
    render_template = 'plugins/footer_brand.html'
    cache = True,
    allow_children = True

    child_classes = [
        'CustomFooterBrandLinkPlugin',
    ]

    def render(self, context, instance, placeholder):
        context = super(CustomFooterBrandPlugin, self).render(context, instance, placeholder)
        return context


@plugin_pool.register_plugin
class CustomFooterBrandLinkPlugin(CMSPluginBase):
    model = CustomFooterBrandLink
    name = _('Бренд: Ссылка')
    render_template = 'plugins/footer_brand_link.html'
    cache = True,
    allow_children = False

    parent_classes = [
        'CustomFooterBrandPlugin',
    ]

    def render(self, context, instance, placeholder):
        context = super(CustomFooterBrandLinkPlugin, self).render(context, instance, placeholder)
        return context


@plugin_pool.register_plugin
class CustomFooterContactPlugin(CMSPluginBase):
    model = CustomFooterContactUs
    name = _('Контакты')
    render_template = 'plugins/footer_contact.html'
    cache = True,
    allow_children = False

    def render(self, context, instance, placeholder):
        context = super(CustomFooterContactPlugin, self).render(context, instance, placeholder)
        return context


@plugin_pool.register_plugin
class CustomFooterQuickLinksPlugin(CMSPluginBase):
    model = CustomFooterQuickLinks
    name = _('Быстрые ссылки')
    render_template = 'plugins/footer_quick_links.html'
    cache = True,
    allow_children = True

    child_classes = [
        'CustomFooterQuickLinksListPlugin',
    ]

    def render(self, context, instance, placeholder):
        context = super(CustomFooterQuickLinksPlugin, self).render(context, instance, placeholder)
        return context


@plugin_pool.register_plugin
class CustomFooterQuickLinksListPlugin(CMSPluginBase):
    model = CustomFooterQuickLinksList
    name = _('Список')
    render_template = 'plugins/footer_quick_links_list.html'
    cache = True,
    allow_children = True

    child_classes = [
        'CustomFooterQuickLinksLinkPlugin',
    ]
    parent_classes = [
        'CustomFooterQuickLinksPlugin',
    ]

    def render(self, context, instance, placeholder):
        context = super(CustomFooterQuickLinksListPlugin, self).render(context, instance, placeholder)
        return context


@plugin_pool.register_plugin
class CustomFooterQuickLinksLinkPlugin(CMSPluginBase):
    model = CustomFooterQuickLinksLink
    name = _('Ссылка')
    render_template = 'plugins/footer_quick_links_link.html'
    cache = True
    allow_children = False

    parent_classes = [
        'CustomFooterQuickLinksListPlugin',
    ]

    def render(self, context, instance, placeholder):
        context = super(CustomFooterQuickLinksLinkPlugin, self).render(context, instance, placeholder)
        return context
