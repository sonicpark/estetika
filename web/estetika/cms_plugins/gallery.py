from cms.plugin_pool import CMSPluginBase, plugin_pool
from django.utils.translation import ugettext_lazy as _

from ..models.pluginmodel.gallery import GalleryPluginModel, GalleryCategoryPluginModel, GalleryImagePluginModel


@plugin_pool.register_plugin
class GalleryPlugin(CMSPluginBase):
    model = GalleryPluginModel
    name = _('Галерея')
    render_template = 'plugins/gallery.html'
    cache = False
    allow_children = True

    child_classes = [
        'GalleryCategoryPlugin'
    ]

    def render(self, context, instance, placeholder):
        context = super(GalleryPlugin, self).render(context, instance, placeholder)
        return context


@plugin_pool.register_plugin
class GalleryCategoryPlugin(CMSPluginBase):
    model = GalleryCategoryPluginModel
    name = _('Категория')
    render_template = 'plugins/gallery_category.html'
    cache = False,
    allow_children = True

    prepopulated_fields = {'slug': ('name',)}

    child_classes = [
        'GalleryImagePlugin'
    ]

    parent_classes = [
        'GalleryPlugin',
    ]

    def render(self, context, instance, placeholder):
        context = super(GalleryCategoryPlugin, self).render(context, instance, placeholder)
        return context


@plugin_pool.register_plugin
class GalleryImagePlugin(CMSPluginBase):
    model = GalleryImagePluginModel
    name = _('Изображение')
    render_template = 'plugins/gallery_image.html'
    cache = False
    allow_children = True

    child_classes = [
        ''
    ]

    parent_classes = [
        'GalleryCategoryPlugin'
    ]

    def render(self, context, instance, placeholder):
        context = super(GalleryImagePlugin, self).render(context, instance, placeholder)
        return context
