from cms.plugin_pool import CMSPluginBase, plugin_pool
from django.utils.translation import ugettext_lazy as _

from ..models.pluginmodel.services import OurService, Service


@plugin_pool.register_plugin
class OurServicesPlugin(CMSPluginBase):
    model = OurService
    name = _('Наши услуги')
    render_template = 'plugins/services.html'
    cache = False
    allow_children = True
    child_classes = ['ServicePlugin']

    def render(self, context, instance, placeholder):
        context = super(OurServicesPlugin, self).render(context, instance, placeholder)
        return context


@plugin_pool.register_plugin
class ServicePlugin(CMSPluginBase):
    model = Service
    name = _('Услуга')
    render_template = 'plugins/services_service.html'
    cache = False
    require_parent = True
    allow_children = False
    parent_classes = ['OurServicesPlugin']

    def render(self, context, instance, placeholder):
        context = super(ServicePlugin, self).render(context, instance, placeholder)
        return context
