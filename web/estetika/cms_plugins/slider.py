from cms.plugin_pool import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext_lazy as _

from ..models.pluginmodel.slider import Slider, Slide, SlideText


@plugin_pool.register_plugin
class SliderPlugin(CMSPluginBase):
    model = Slider
    name = _('Слайдер')
    render_template = 'plugins/slider.html'
    cache = True
    allow_children = True
    child_classes = ['SlidePlugin']

    def render(self, context, instance, placeholder):
        context = super(SliderPlugin, self).render(context, instance, placeholder)
        return context


@plugin_pool.register_plugin
class SlidePlugin(CMSPluginBase):
    model = Slide
    name = _('Слайдер: Слайд')
    render_template = 'plugins/slide.html'
    cache = True,
    require_parent = True
    allow_children = True
    child_classes = ['SlideText']
    parent_classes = ['SliderPlugin']

    fieldsets = (
        (None, {
            'fields': ('data_title',)
        }),
        (_('Изображение'), {
            'fields': ('data_image',)
        }),
        (_('Настройки'), {
            'classes': ('collapse',),
            'fields': (('data_transition', 'data_master_speed'), ('data_position_horizontal', 'data_position_vertical')),
        }),
    )

    def render(self, context, instance, placeholder):
        context = super(SlidePlugin, self).render(context, instance, placeholder)
        return context


@plugin_pool.register_plugin
class SlideText(CMSPluginBase):
    model = SlideText
    name = _('Слайдер: Текст')
    render_template = 'plugins/slide_text.html'
    cache = True,
    require_parent = True
    parent_classes = ['SlidePlugin']

    fieldsets = (
        (_('Основые'), {
            'fields': ('data_text', ('data_size', 'data_color',),)
        }),
        (_('Дополнительно'), {
            'classes': ('collapse',),
            'fields': (
                ('data_x', 'data_hoffset'),
                ('data_y', 'data_voffset'),
                ('data_speed', 'data_start', 'data_easing',),
                ('data_splitin', 'data_splitout',),
                ('data_elementdelay', 'data_endelementdelay',),
                ('data_endspeed', 'data_endeasing',)
            )
        }),
    )

    def render(self, context, instance, placeholder):
        context = super(SlideText, self).render(context, instance, placeholder)
        return context
