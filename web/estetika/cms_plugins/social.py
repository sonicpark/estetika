from cms.plugin_pool import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext_lazy as _

from ..models.pluginmodel.social import SocialNetworks, Network


@plugin_pool.register_plugin
class SocialNetworksPlugin(CMSPluginBase):
    model = SocialNetworks
    name = _('Социальные сети')
    render_template = 'plugins/footer_social.html'
    cache = True,
    allow_children = True
    child_classes = ['NetworkPlugin']

    def render(self, context, instance, placeholder):
        context = super(SocialNetworksPlugin, self).render(context, instance, placeholder)
        return context


@plugin_pool.register_plugin
class NetworkPlugin(CMSPluginBase):
    model = Network
    name = _('Социальная сеть')
    render_template = 'plugins/footer_social_network.html'
    cache = True
    require_parent = True
    allow_children = False
    parent_classes = ['SocialNetworksPlugin']

    def render(self, context, instance, placeholder):
        context = super(NetworkPlugin, self).render(context, instance, placeholder)
        return context
