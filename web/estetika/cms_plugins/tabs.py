from cms.plugin_pool import CMSPluginBase, plugin_pool
from django.utils.translation import ugettext_lazy as _

from ..models.pluginmodel.tabs import TextsTabs, TextsTab


@plugin_pool.register_plugin
class TextTabsPlugin(CMSPluginBase):
    model = TextsTabs
    name = _('Табы')
    render_template = 'plugins/tabs.html'
    cache = False
    allow_children = True

    child_classes = [
        'TextTabPlugin',
    ]

    def render(self, context, instance, placeholder):
        context = super(TextTabsPlugin, self).render(context, instance, placeholder)
        return context


@plugin_pool.register_plugin
class TextTabPlugin(CMSPluginBase):
    model = TextsTab
    name = _('Таб: Текстовой')
    render_template = 'plugins/tabs_text_tab.html'
    cache = False
    allow_children = False

    parent_classes = [
        'TextTabsPlugin',
    ]

    def render(self, context, instance, placeholder):
        context = super(TextTabPlugin, self).render(context, instance, placeholder)
        return context
