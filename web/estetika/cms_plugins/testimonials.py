from cms.plugin_pool import CMSPluginBase, plugin_pool
from django.utils.translation import ugettext_lazy as _

from ..models.pluginmodel.testimonials import TestimonialsPluginModel, Testimonial


@plugin_pool.register_plugin
class TestimonialsPlugin(CMSPluginBase):
    model = TestimonialsPluginModel
    name = _('Отзывы')
    render_template = 'plugins/testimonials.html'
    cache = False,
    allow_children = False

    def render(self, context, instance, placeholder):
        context = super(TestimonialsPlugin, self).render(context, instance, placeholder)
        testimonials = Testimonial.objects.filter(publish=True)
        context.update({
            'testimonials': testimonials
        })
        return context
