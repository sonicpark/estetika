from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models.pluginmodel import CMSPlugin
from djangocms_picture.models import FilerImageField


"""

"""


class AboutPluginModel(CMSPlugin):
    title = models.CharField(
        verbose_name='Заголовок',
        blank=False,
        null=False,
        max_length=100,
    )
    sub_title = models.CharField(
        verbose_name='Подзаголовок',
        blank=True,
        null=True,
        max_length=100,
    )
    text = models.TextField(
        verbose_name='Описание',
        blank=False,
        null=False,
    )
    image = FilerImageField(
        verbose_name='Изображение',
        related_name='about_image',
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name = 'Плагин: О нас'
        verbose_name_plural = 'Плагины: О нас'

    def __str__(self):
        return f'{self._meta.verbose_name}: {self.title}'
