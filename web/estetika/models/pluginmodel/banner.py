from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models.pluginmodel import CMSPlugin
from djangocms_picture.models import FilerImageField


"""
Плагин статического банера страницы
"""


# Основная модель Статичного банера.
class PageBanner(CMSPlugin):
    name = models.CharField(
        verbose_name=_('Заголовок'),
        blank=True,
        null=True,
        max_length=50,
    )
    image = FilerImageField(
        verbose_name=_('Изображение'),
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name='banner_image',
        help_text='Размер: 1550px х 155px',
    )

    class Meta:
        verbose_name = 'Статичный Банер'
        verbose_name_plural = 'Статичные Банера'

    def __str__(self):
        return f'{self._meta.verbose_name}: {self.name}'
