from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models.pluginmodel import CMSPlugin
from djangocms_text_ckeditor.fields import HTMLField
from djangocms_picture.models import FilerImageField


class CurvedSection(CMSPlugin):
    name = models.CharField(
        verbose_name='Заголовок',
        max_length=150,
    )
    sub_name = models.CharField(
        verbose_name='Подзагловок',
        max_length=150,
    )
    text = HTMLField(
        verbose_name='Текст'
    )
    image = FilerImageField(
        verbose_name='Изображение',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name = 'Косая Секция'
        verbose_name_plural = 'Косые Секции'

    def __str__(self):
        return f'{self.sub_name}: {self.name}'
