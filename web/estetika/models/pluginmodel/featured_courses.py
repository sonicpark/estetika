from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models.pluginmodel import CMSPlugin
from cms.models import Page
from djangocms_picture.models import FilerImageField


"""

"""
FADE_IN = (
    ('fadeInLeft', 'fadeInLeft'),
    ('fadeInRight', 'fadeInRight'),
    ('fadeInUp', 'fadeInUp'),
)
ARROW = (
    ('from-right', 'from-right'),
    ('from-left', 'from-left'),
)


class Featured(CMSPlugin):
    image = FilerImageField(
        verbose_name=_('Изображение'),
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        help_text="1550px-600px",
    )

    class Meta:
        verbose_name = 'Список услуг'
        verbose_name_plural = 'Списки услуг'

    def __str__(self):
        return f'{self._meta.verbose_name}'


class FeaturedPost(CMSPlugin):
    name = models.CharField(
        verbose_name=_('Название'),
        max_length=200,
    )
    desc = models.CharField(
        verbose_name=_('Описание'),
        max_length=300,
    )
    page = models.ForeignKey(
        verbose_name=_('Связанная страница'),
        to=Page,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    image = FilerImageField(
        verbose_name=_('Изображение'),
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
    )

    data_fade = models.CharField(
        verbose_name=_('Тип повяления'),
        max_length=100,
        choices=FADE_IN,
    )

    data_arrow = models.CharField(
        verbose_name=_('Направление'),
        max_length=100,
        choices=ARROW,
    )

    data_wow_delay = models.FloatField(
        verbose_name=_('Задержка появления'),
        default=0.5
    )
    @property
    def wow_delay(self):
        return f'{self.data_wow_delay}'.replace(',', '.')

    data_wow_durations = models.FloatField(
        verbose_name=_('Продолжение появления'),
        default=1,
    )
    @property
    def wow_durations(self):
        return f'{self.data_wow_durations}'.replace(',', '.')

    data_wow_offset = models.FloatField(
        verbose_name=_('Смещение появления'),
        default=0
    )

    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Услуги'

    def __str__(self):
        return self.name
