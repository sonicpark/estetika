from cms.models import PlaceholderField
from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models.pluginmodel import CMSPlugin
from djangocms_picture.models import FilerImageField

"""
Плагин Настраиваемый Футер
"""


# Основная модель Футера
class CustomFooter(CMSPlugin):
    class Meta:
        verbose_name = 'Футер'
        verbose_name_plural = 'Футер'

    def __str__(self):
        return f'{self._meta.verbose_name}'


class CustomFooterUpper(CMSPlugin):
    pass


class CustomFooterLower(CMSPlugin):
    pass


# Основная модель Брендированного блока
class CustomFooterBrand(CMSPlugin):
    name = models.CharField(
        verbose_name='Название сайта',
        max_length=150,
    )
    logo = FilerImageField(
        verbose_name='Логотип',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    logo_width = models.PositiveIntegerField(
        verbose_name='Логотип: Ширина',
        default=100,
    )
    logo_height = models.PositiveIntegerField(
        verbose_name='Логотип: Высота',
        default=100,
    )

    class Meta:
        verbose_name = 'Копирайтинг'
        verbose_name_plural = 'Копирайтинги'

    def __str__(self):
        return f'{self._meta.verbose_name}'


# Модель Ссылок Бренда
class CustomFooterBrandLink(CMSPlugin):
    name = models.CharField(
        verbose_name='Название ссылки',
        max_length=20,
    )
    link = models.URLField(
        verbose_name='Url Ссылки',
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name = 'Брендированная ссылка',
        verbose_name_plural = 'Брендированные ссылки'

    def __str__(self):
        return f'{self._meta.verbose_name}: {self.name}'


# Основная модель Быстрых ссылок
class CustomFooterQuickLinks(CMSPlugin):
    name = models.CharField(
        verbose_name='Заголовок',
        max_length=100,
    )

    class Meta:
        verbose_name = 'Футер: Быстрые ссылки'
        verbose_name_plural = 'Футер: Быстрые ссылки'

    def __str__(self):
        return f'{self._meta.verbose_name}'


# Модель Плагина списка быстрых ссылок
class CustomFooterQuickLinksList(CMSPlugin):
    name = models.CharField(
        verbose_name='Название',
        max_length=20,
    )

    class Meta:
        verbose_name = 'Список ссылок'
        verbose_name_plural = 'Списки ссылок'

    def __str__(self):
        return f'{self._meta.verbose_name}: {self.name}'


# Модель Плагина Быстрой ссылки
class CustomFooterQuickLinksLink(CMSPlugin):
    name = models.CharField(
        verbose_name='Название',
        max_length=20,
    )
    link = models.URLField(
        verbose_name='URL Адрес',
    )

    class Meta:
        verbose_name = 'Ссылка'
        verbose_name_plural = 'Ссылки'

    def __str__(self):
        return f'{self._meta.verbose_name}'


# Основная модель популярных тегов
class CustomFooterPopularTags(CMSPlugin):
    class Meta:
        verbose_name = ''
        verbose_name_plural = ''

    def __str__(self):
        return f'{self._meta.verbose_name}'


# ОСновная моедель контактов в Футере
class CustomFooterContactUs(CMSPlugin):
    name = models.CharField(
        verbose_name='Заголовок',
        max_length=50,
    )
    email = models.EmailField(
        verbose_name='Email',
    )
    phone = models.CharField(
        verbose_name='Телефон',
        max_length=15,
    )
    address = models.TextField(
        verbose_name='Адрес',
    )

    class Meta:
        verbose_name = 'Футер: Контакты'
        verbose_name_plural = 'Футер: Контакты'

    def __str__(self):
        return f'{self._meta.verbose_name}'
