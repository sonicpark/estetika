from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models.pluginmodel import CMSPlugin
from djangocms_picture.models import FilerImageField


class GalleryPluginModel(CMSPlugin):
    name = models.CharField(
        verbose_name='Заголовок',
        max_length=150,
    )
    sub_name = models.CharField(
        verbose_name='Подзаголовок',
        max_length=150,
    )

    class Meta:
        verbose_name = 'Галерея'
        verbose_name_plural = 'Галерея'

    def __str__(self):
        return f'{self._meta.verbose_name}: {self.name}'


class GalleryCategoryPluginModel(CMSPlugin):
    name = models.CharField(
        verbose_name='Название',
        max_length=50,
    )
    slug = models.SlugField(
        verbose_name='Фильтр',
    )

    class Meta:
        verbose_name = 'Категория изображений'
        verbose_name_plural = 'Категории изображений'

    def __str__(self):
        return f'{self.name}'


class GalleryImagePluginModel(CMSPlugin):
    name = models.CharField(
        verbose_name='Название',
        max_length=150,
    )
    image = FilerImageField(
        verbose_name='Изображение',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name = 'Изображение'
        verbose_name_plural = 'Изображения'

    def __str__(self):
        return f'{self.name}'
