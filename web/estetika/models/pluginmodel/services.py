from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models.pluginmodel import CMSPlugin


"""
Плагин Наши Услуги
"""


class OurService(CMSPlugin):
    class Meta:
        verbose_name = 'Плагин: Наши услуги'
        verbose_name_plural = 'Плагины: Наши услуги'


class Service(CMSPlugin):
    title = models.CharField(
        verbose_name='Заголовок',
        max_length=100,
        blank=False,
        null=False,
    )
    sub_title = models.CharField(
        verbose_name='Подзаголовок',
        max_length=100,
        blank=False,
        null=False,
    )
    description = models.TextField(
        verbose_name='Описание',
        blank=False,
        null=False,
    )

    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Все услуги'

    def __str__(self):
        return f'{self._meta.verbose_name}'
