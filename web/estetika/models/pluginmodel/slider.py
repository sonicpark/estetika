from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models.pluginmodel import CMSPlugin
from djangocms_picture.models import FilerImageField


"""
Плагин Настраиваемый слайдер
"""


# Основная модель Слайдера
class Slider(CMSPlugin):
    class Meta:
        verbose_name = 'Эстетика: Слайдер'
        verbose_name_plural = 'Эстетика: Слайдеры'

    def __str__(self):
        return f'{self._meta.verbose_name}'


DataTransition = (
    ('fade', 'Проявление'),
    ('slidedown', 'Скольжение снизу'),
    ('slideup', 'Скольжение сверху'),
)
DataPositionH = (
    ('left', 'Слева'),
    ('center', 'По центру'),
    ('right', 'Справа'),
)
DataPositionV = (
    ('top', 'Сверху'),
    ('center', 'По центру'),
    ('bottom', 'Справа')
)


# Модель слайда для слайдера
class Slide(CMSPlugin):
    data_title = models.CharField(
        verbose_name='Заголовок',
        max_length=30
    )
    data_transition = models.CharField(
        verbose_name='Тип перехода',
        max_length=20,
        default='fade',
        choices=DataTransition,
    )
    data_master_speed = models.PositiveIntegerField(
        verbose_name='Скорость перехода',
        default=1000,
        blank=True,
    )
    data_image = FilerImageField(
        verbose_name='Изображение',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    data_position_horizontal = models.CharField(
        verbose_name='Расположение: По горизонтали',
        max_length=50,
        choices=DataPositionH,
    )
    data_position_vertical = models.CharField(
        verbose_name='Расположение: По вертикали',
        max_length=50,
        choices=DataPositionV,
    )

    class Meta:
        verbose_name = 'Слайд'
        verbose_name_plural = 'Слайды'

    def __str__(self):
        return f'{self.data_title}'


DataSize = (
    ('h1', 'h1'),
    ('h2', 'h2'),
    ('h3', 'h3'),
    ('h4', 'h4'),
)
DataColor = (
    ('light-blue', 'Голубой'),
    ('dark-blue', 'Синий'),
    ('light-pink', 'Оранжевый'),
)
DataX = (
    ('left', 'Слева'),
    ('right', 'Справа'),
    ('center', 'По Центру'),
)
DataY = (
    ('center', 'По Центру'),
)
DataEasing = (
    ('easeOutExpo', 'easeOutExpo'),
)
DataEndEasing = (
    ('Power4.easeIn', 'Power4.easeIn'),
)


# Настраиваемый текст для слайда
class SlideText(CMSPlugin):
    data_text = models.CharField(
        verbose_name='Текст',
        max_length=20
    )
    data_size = models.CharField(
        verbose_name='Заголовок',
        max_length=5,
        default='h3',
        choices=DataSize,
    )
    data_color = models.CharField(
        verbose_name='Цвет фона',
        max_length=20,
        default='light-blue',
        choices=DataColor,
    )
    data_x = models.CharField(
        max_length=20,
        default='left',
        choices=DataX,
        verbose_name='Начальная точка: X',
        help_text=_('Расположение начальной точки координат по оси "X".')
    )
    data_hoffset = models.IntegerField(
        default=500,
        verbose_name=_('Смещение по горизонтали'),
        help_text=_('Смещение по горизонтали относительно начальной точки координат по оси "X".')
    )
    data_y = models.CharField(
        max_length=20,
        default='center',
        choices=DataY,
        verbose_name='Начальная точка: Y',
        help_text=_('Расположение начальной точки координат по оси "Y".'),
    )
    data_voffset = models.IntegerField(
        default=-100,
        verbose_name=_('Смещение по вертикали'),
        help_text=_('Смещение по вертикали относительно начальной точки координат по оси "Y".')
    )
    data_speed = models.IntegerField(
        default=1500,
    )
    data_start = models.IntegerField(
        default=500,
    )
    data_easing = models.CharField(
        max_length=20,
        default='easeOutExpo',
        choices=DataEasing,
    )
    data_splitin = models.CharField(
        max_length=20,
        default='none'
    )
    data_splitout = models.CharField(
        max_length=20,
        default='none'
    )
    data_elementdelay = models.FloatField(
        default=0.01
    )
    data_endelementdelay = models.FloatField(
        default=0.3
    )
    data_endspeed = models.IntegerField(
        default=1200
    )
    data_endeasing = models.CharField(
        max_length=20,
        default='Power4.easeIn',
        choices=DataEndEasing,
    )

    class Meta:
        verbose_name = 'Текст'
        verbose_name_plural = 'Тексты'

    def __str__(self):
        return f'{self.data_text}'
