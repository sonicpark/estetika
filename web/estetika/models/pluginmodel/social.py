from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models.pluginmodel import CMSPlugin
from djangocms_picture.models import FilerImageField

"""
Плагин с Ссылками на социальные сети.
"""


# Основная модель социальных сетей
class SocialNetworks(CMSPlugin):
    name = models.CharField(
        verbose_name=_('Заголовок'),
        blank=False,
        null=False,
        max_length=30,
    )

    class Meta:
        verbose_name = 'Эстетика: Социальные сети'
        verbose_name_plural = 'Эстетика: Социальные сети'

    def __str__(self):
        return f'{self._meta.verbose_name}'


NetworkName = (
    ('fa-facebook-f', 'Facebook'),
    ('fa-twitter', 'Twitter'),
    ('fa-dribbble', 'Dribbble'),
    ('fa-google', 'Google'),
    ('fa-pinterest', 'Pinterest'),
    ('fa-instagram', 'Instagram'),
)


# Моедль отобрадаемых социальных сетей.
class Network(CMSPlugin):
    name = models.CharField(
        verbose_name=_('Название'),
        blank=False,
        null=False,
        choices=NetworkName,
        max_length=50
    )
    link = models.URLField(
        verbose_name=_('Ссылка'),
        blank=False,
        null=False,
    )
    delay = models.DecimalField(
        verbose_name='Задержка',
        default=0,
        max_digits=4,
        decimal_places=2
    )
    @property
    def get_delay(self):
        return f'{self.delay}'.replace(',', '.')

    class Meta:
        verbose_name = _('Социальная сеть')
        verbose_name_plural = _('Социальные сети')

    def __str__(self):
        return f'{self.get_name_display()}'
