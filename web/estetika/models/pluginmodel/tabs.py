from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models.pluginmodel import CMSPlugin
from djangocms_text_ckeditor.fields import HTMLField


"""
Плагин Текстовые Табы
"""


# Основная модель Табов
class TextsTabs(CMSPlugin):
    class Meta:
        verbose_name = 'Табы'
        verbose_name_plural = 'Табы'

    def __str__(self):
        return f'{self._meta.verbose_name}'


#
class TextsTab(CMSPlugin):
    name = models.CharField(
        verbose_name='Название',
        max_length=100,
    )
    text = HTMLField(
        blank=True,
    )

    class Meta:
        verbose_name = 'Таб: Текст'
        verbose_name_plural = 'Таб: Тексты'

    def __str__(self):
        return f'{self._meta.verbose_name}'
