from django.db import models
from django.utils.translation import ugettext_lazy as _
from cms.models.pluginmodel import CMSPlugin
from djangocms_picture.models import FilerImageField


"""
Плагин с Отзывами клиентов.
"""


class Testimonial(models.Model):
    first_name = models.CharField(
        verbose_name=_('Имя:'),
        max_length=50,
        blank=False,
        null=False,
    )
    last_name = models.CharField(
        verbose_name=_('Фамилия:'),
        max_length=50,
        blank=False,
        null=False,
    )
    message = models.TextField(
        verbose_name=_('Отзыв:'),
        blank=True,
        null=True,
        max_length=200,
    )
    image = FilerImageField(
        verbose_name=_('Фото'),
        blank=False,
        null=False,
        related_name='testimonial_image',
        on_delete=models.CASCADE,
        help_text='150px x 150px'
    )
    publish = models.BooleanField(
        verbose_name=_('Опубликовано'),
        default=False,
    )

    class Meta:
        verbose_name = _('Отзыв')
        verbose_name_plural = _('Отзывы')

    def __str__(self):
        return f'{self._meta.verbose_name}'


class TestimonialsPluginModel(CMSPlugin):
    title = models.CharField(
        verbose_name=_('Заголовок'),
        max_length=50,
    )
    sub_title = models.CharField(
        verbose_name=_('Подзаголовок'),
        max_length=100,
    )
    maximum = models.PositiveIntegerField(
        verbose_name=_('Максимум'),
        default=6,
        blank=True,
    )

    class Meta:
        verbose_name = _('Плагин: Отзывы')
        verbose_name_plural = _('Плагины: Отзывы')

    def __str__(self):
        return f'{self._meta.verbose_name}'
